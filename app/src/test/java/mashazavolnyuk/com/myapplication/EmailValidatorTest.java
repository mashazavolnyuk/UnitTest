package mashazavolnyuk.com.myapplication;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import java.util.regex.Pattern;

import mashazavolnyuk.com.myapplication.util.ValidNumber;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
/**
 * Created by mashka on 22.08.17.
 */

public class EmailValidatorTest {

    @Test
    public void emailValidator_CorrectEmailSimple_ReturnsTrue() {
        assertThat(ValidNumber.isPositiveNumber(1), is(true));
        assertThat(ValidNumber.isPositiveNumber(0), is(true));
    }
}
